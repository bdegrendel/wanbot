package com.degrendel.wanbot.output

data class Halt(val author: String = "", val reason: String = ""): Command