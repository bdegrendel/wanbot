package com.degrendel.wanbot.nlp

data class Word(val id: Int, val keyword: String, val token: String, val tag: String, val lemma: String, val chunk: String)
