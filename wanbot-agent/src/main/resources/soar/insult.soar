# Rules governing the insult goal

echo "Loading insult.soar..."

# Insult goals must have the format of:
# (TopState ^goals.insult: Goal)
# Goal: (
#   ^header: Header [required])
#     - Header of what message triggered this insult.
assert-singular goals wanbot goals.insult header

#   ^user: Integer [required]
#     - The id of the targeted user
assert-singular   goals wanbot goals.insult user
assert-is-user-id goals wanbot goals.insult user

#   ^word: String [required]
#     - What to call this user.
assert-singular goals wanbot goals.insult word
# )

# Propose:
#   Enter insult substate on active insult goal.
sp {insult*propose*substate
   (state <s> ^name wanbot ^goals.insult <insult>)
   (<insult> ^meta.active *yes* ^header <header> ^user <user> ^word <word>)
-->
   (<s> ^operator <o> + =)
   (<o> ^name insult ^substate *yes* ^goal <insult> ^results <results> ^arguments <args>)
   (<args> ^header <header> ^word <word> ^user <user>)
}

# Apply:
#   Mark insult goal as accomplished, regardless whether the agent has responded further.
sp {insult*apply*substate*complete
   (state <s> ^name wanbot ^operator <o>)
   (<o> ^name insult ^results.done *yes* ^goal.meta <meta>)
-->
   (<meta> ^achieved.why |Insult pondered|)
}
   
# Apply:
#   Dispense determined message.
sp {insult*apply*substate
   (state <s> ^name wanbot ^operator <o> ^io.output-link <out>)
   (<o> ^name insult ^goal.meta <meta> ^results <results>)
   (<results> ^done *yes* ^response <response> ^server <server> ^channel <channel>)
-->
   (<out> ^recite <m>)
   (<m> ^sentence <response> ^server <server> ^channel <channel>)
}

# Propose/Apply: Create action for this substate
propose-apply-start-goal-action insult "Determine what/where to issue insult."

# Elaborate:
#   Find the target's user tree.
sp {insult*elaborate*target
   (state <s> ^name insult ^user <id> ^facts.users.user <target>)
   (<target> ^id <id>)
-->
   (<s> ^target <target>)
}

# Elaborate:
#   Find the sender's user tree.
sp {insult*elaborate*sender
   (state <s> ^name insult ^header <header> ^facts.users.user <sender>)
   (<header> ^server <server> ^channel <channel>)
   (<sender> ^instance <instance>)
   (<instance> ^server <server> ^channel <channel>)
-->
   (<s> ^sender <sender>)
}

# Propose:
#   Ignore if neither the targeted user nor the sender is online to hear the noise.
sp {insult*propose*ignore
   (state <s> ^name insult ^target <target> ^sender <sender> ^results <results> ^action <action>)
   (<target> -^online *yes*)
   (<sender> -^online *yes*)
-->
   (<s> ^operator <o> +)
   (<o> ^name ignore-insult ^results <results> ^action <action>)
}

# Apply:
#   If an insult is dispensed in a channel and no one is around to hear it, does it produce any salt?
sp {insult*apply*ignore
   (state <s> ^name insult ^operator <o>)
   (<o> ^name ignore-insult ^results <results> ^action <action>)
-->
   (<results> ^done *yes*)
   (<action> ^step |Ignored since neither sender nor target are around to hear the noise|)
}

# Propose:
#   Repond if the targeted user isn't online to hear the noise.
sp {insult*propose*target-not-online
   (state <s> ^name insult ^user <id> ^header <header> ^results <results> ^target <target> ^sender <sender>
              ^action <action>)
   (<target> -^online *yes*)
   (<sender> ^instance <instance>)
   (<instance> ^online *yes*)
-->
   (<s> ^operator <o> + =)
   (<o> ^name target-not-online ^header <header> ^results <results> ^instance <instance> ^target <target>
        ^sender <sender> ^action <action>)
}

# Prefer:
#   Prefer responding to sender on same channel they issued the request.
sp {insult*prefer*target-not-online*on*original*channel
   (state <s> ^name insult ^operator <o1> + ^operator <o2> +)
   (<o1> ^name target-not-online ^header <header> ^instance <instance>)
   (<header> ^server <server> ^channel <channel>)
   (<instance> ^server <server> ^channel <channel>)
   (<o2> ^name target-not-online -^instance <instance>)
-->
   (<s> ^operator <o1> > <o2>)
}

# Apply:
#   Issue notice to sender if target isn't online.
sp {insult*apply*target-not-online
   (state <s> ^name insult ^operator <o>)
   (<o> ^name target-not-online ^instance <instance> ^sender.name <sender> ^target.name <target> ^results <results>
        ^action <action>)
   (<instance> ^server <server> ^channel <channel>)
-->
   (<action> ^step |Issued response to sender that target is nowhere to be found|)
   (<results> ^done *yes* ^server <server> ^channel <channel>
              ^response (concat |ATTN: | <sender> |, RE: INSULT.  | <target> | NOT FOUND, INSULT.... BELAYED.|))
}

# Propose:
#   Target is active, transmit the insult.
sp {insult*propose*dispense
   (state <s> ^name insult ^results <results> ^target <target> ^header <header> ^word <word> ^action <action>)
   (<target> ^online *yes* ^instance <instance>)
-->
   (<s> ^operator <o> + =)
   (<o> ^name dispense ^target <target> ^results <results> ^header <header> ^word <word>
        ^instance <instance> ^action <action>)
}

# Prefer:
#   Prefer insulting user on same channel as where the request was issued.
sp {insult*prefer*dispense*to*requesting*channel
   (state <s> ^name insult ^operator <o1> + ^operator <o2> +)
   (<o1> ^name dispense ^header <header> ^instance <instance>)
   (<header> ^server <server> ^channel <channel>)
   (<instance> ^server <server> ^channel <channel>)
   (<o2> ^name dispense -^instance <instance>)
-->
   (<s> ^operator <o1> > <o2>)
}

# Apply:
#   Dispense with extreme insult.  If it's not us.
sp {insult*apply*dispense
   (state <s> ^name insult ^operator <o>)
   (<o> ^name dispense ^instance <instance> ^results <results> ^action <action> ^word <word>
        ^target.name { <> |wanbot| <target>})
   (<instance> ^server <server> ^channel <channel>)
-->
   (<action> ^step |Issued insult to target|)
   (<results> ^done *yes* ^server <server> ^channel <channel>
              ^response (concat |Hey | <target> |, you're a fuckin' | <word> |!|))
}

# Apply:
#   Give yourself a high five, you've earned it.
sp {insult*apply*dispense*self*override
   (state <s> ^name insult ^operator <o>)
   (<o> ^name dispense ^instance <instance> ^results <results> ^action <action> ^target.name |wanbot|)
   (<instance> ^server <server> ^channel <channel>)
-->
   (<action> ^step |Issued high-five to self|)
   (<results> ^done *yes* ^server <server> ^channel <channel>
              ^response |Hey WanBot, you're a fuckin'..... really cool bot.  Keep it up!|)
}
  
