package com.degrendel.wanbot.agent

import com.degrendel.wanbot.common.logger
import org.jsoar.kernel.io.InputOutput
import org.jsoar.kernel.io.InputWme
import org.jsoar.kernel.io.InputWmes
import org.jsoar.kernel.symbols.Identifier
import java.util.*

class InputWMEs(private val io: InputOutput, private val debuggingMode: Boolean = false)
{
  companion object
  {
    val L by logger()
    const val TRUE = "*yes*"
    const val FALSE = "*no*"
  }

  private val wmes = HashSet<InputWme>()

  fun <T : Any> add(node: String, data: T, klass: Class<T>)
  {
    val existing = HashMap<Any, Identifier>()
    val wme = performAdd(existing, io.inputLink, node, data, klass)
    if (wme == null)
      L.warn("Tried to push {}, didn't generate any input", data)
    else
    {
      L.info("Adding {}", wme)
      wmes.add(wme)
    }
  }

  private fun <T : Any> performAdd(existing: HashMap<Any, Identifier>, parent: Identifier, node: String, data: T,
                                   klass: Class<T>): InputWme?
  {
    val interfaces = klass.interfaces
    // NOTE: Unclear why boxed primitives need javaObjectType, but String is A-OK with the standard class.java value
    return if (klass == Int::class.javaObjectType || klass == Long::class.javaObjectType || klass == String::class.java
        || klass == Double::class.javaObjectType)
    {
      if (debuggingMode)
        L.info("Adding primitive ({} ^{} {})", parent, node, data)
      InputWmes.add(io, parent, node, data)
    }
    else if (klass == Boolean::class.javaObjectType)
    {
      val value = if (data as Boolean) TRUE else FALSE
      if (debuggingMode)
        L.info("Adding primitive ({} ^{} {})", parent, node, value)
      InputWmes.add(io, parent, node, value)
    }
    else if (data in existing)
    {
      if (debuggingMode)
        L.info("Adding existing ({} ^{} {})", parent, node, existing[data])
      InputWmes.add(io, parent, node, existing[data]!!)
    }
    else if (List::class.java in interfaces)
    {
      val type = removePlural(node)
      @Suppress("UNCHECKED_CAST")
      val length = (data as List<Any>).size
      var previous: InputWme? = null
      for (i in 0 until length)
      {
        val child = data[i]
        val wme = performAdd(existing, parent, type, child, child.javaClass)
        if (wme != null)
        {
          val iden: Identifier = wme.value.asIdentifier() ?: continue
          if (previous == null)
            InputWmes.add(io, iden, "first", TRUE)
          else
          {
            InputWmes.add(io, iden, "previous", previous.value)
            InputWmes.add(io, previous.value.asIdentifier(), "next", iden)
          }
          if (i == length - 1)
            InputWmes.add(io, iden, "last", TRUE)
          previous = wme
        }
      }
      null
    }
    else
    {
      val identifier = io.symbols.createIdentifier(node[0])
      if (debuggingMode)
        L.info("Adding complex ({} ^{} {})", parent, node, identifier)
      val result = InputWmes.add(io, parent, node, identifier)
      for (method in klass.methods)
      {
        // Getters only
        val name = method.name
        if (method.parameterCount != 0 || method.returnType == Void.TYPE || !name.startsWith("get")
            || name == "getClass")
          continue

        val type = removeUppercase(name.substring(3))
        if (type.isBlank())
          continue
        val value = method.invoke(data)
        performAdd(existing, identifier, type, value, value.javaClass)
      }
      existing.put(data, identifier)
      result
    }
  }

  private fun removePlural(name: String): String
  {
    return when
    {
      name.endsWith("ies") -> name.substring(0, name.length - 3) + "y"
      name.endsWith("s") -> name.substring(0, name.length - 1)
      else -> name
    }
  }

  private fun removeUppercase(name: String): String
  {
    if (name.isBlank()) return name
    val first = name[0].toLowerCase()
    val result = first + name.substring(1)
    return result.replace(Regex("[A-Z]"), { match: MatchResult -> "-" + match.value.toLowerCase() })
  }

  fun update()
  {
    val toRemove = HashSet<InputWme>()
    for (input in wmes)
    {
      for (wme in input.children)
      {
        if (wme.attribute.asString().value == "status" && wme.value.asString().value == "processed")
          toRemove.add(input)
      }
    }
    for (remove in toRemove)
    {
      L.info("Dropping {}", remove)
      remove.remove()
      wmes.remove(remove)
    }
  }
}
