package com.degrendel.wanbot.input

data class Header(val server: String, val user: Long = 0, val channel: Long = 0)
