package com.degrendel.wanbot.server

import com.degrendel.wanbot.WanBot
import com.degrendel.wanbot.common.logger
import com.degrendel.wanbot.input.Header
import com.degrendel.wanbot.input.UserStatus
import sx.blah.discord.api.ClientBuilder
import sx.blah.discord.api.IDiscordClient
import sx.blah.discord.api.events.EventSubscriber
import sx.blah.discord.handle.impl.events.ReadyEvent
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent
import sx.blah.discord.handle.impl.events.user.PresenceUpdateEvent
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.StatusType
import sx.blah.discord.util.DiscordException
import sx.blah.discord.util.MessageBuilder
import sx.blah.discord.util.MissingPermissionsException
import sx.blah.discord.util.RateLimitException

class DiscordServer(parent: WanBot, token: String, private val voice: Boolean) : Server(parent,
    Server.ServerType.DISCORD)
{
  companion object
  {
    val L by logger()
    const val VOICE_CHANNEL = "general"
  }

  private val client: IDiscordClient

  init
  {
    enableMagicCommands(Server.MagicCommands.PING, Server.MagicCommands.INCUBATE)

    val builder = ClientBuilder()
    builder.withToken(token)
    client = builder.login()
    client.dispatcher.registerListener(this)
  }

  @EventSubscriber
  fun onReadyEvent(event: ReadyEvent)
  {
    L.warn("WANBOT IS CONNECTED TO DISCORD")
    L.warn("PREPARE YOUR BUTTS")
    for (guild in event.client.guilds)
    {
      L.warn("WANBOT IS INFECTING SERVER {}", guild.name)
      for (user in guild.users)
      {
        parent.agent.input.add(UserStatus(Header(ServerType.DISCORD.toString(), user.longID, guild.longID),
            parent.agent.users.lookup(user.name), user.name.toLowerCase(), user.name,
            isOnline(user.presence.status)))
      }
      if (voice)
      {
        for (voice in guild.voiceChannels)
        {
          if (voice.name.equals(VOICE_CHANNEL, true))
          {
            L.warn("--> JOINING VOICE {}", voice.name)
            voice.join()
          }
          else
            L.warn("--> IGNORING VOICE {}", voice.name)
        }
      }
    }
  }

  private fun isOnline(status: StatusType): Boolean
  {
    return when (status)
    {
      StatusType.ONLINE -> true
      StatusType.DND -> true
      StatusType.IDLE -> true
      StatusType.INVISIBLE -> true
      StatusType.OFFLINE -> false
      StatusType.UNKNOWN -> false
      else -> {
        L.warn("Unknown/unhandled user status type: {}", status)
        false
      }
    }
  }

  @EventSubscriber
  fun onPresenceUpdateEvent(event: PresenceUpdateEvent)
  {
    // TODO: This needs to be rethought
    parent.agent.input.add(UserStatus(Header(ServerType.DISCORD.toString(), 0, event.user.longID),
        parent.agent.users.lookup(event.user.name), event.user.name.toLowerCase(), event.user.name,
        isOnline(event.newPresence.status)))
  }

  override fun start()
  {
  }

  override fun stop()
  {
    client.logout()
  }

  override fun speak(message: String, channel: Long)
  {
    val id: IChannel? = client.getChannelByID(channel)
    if (id == null)
    {
      L.error("UNABLE TO FIND CHANNEL {}", channel)
      return
    }
    try
    {
      MessageBuilder(client).withChannel(id).withContent(message).send()
    }
    catch (e: RateLimitException)
    {
      L.warn("WANBOT IS RATE LIMITED WHILE TALKING ON {}", serverNameFromChannel(id))
    }
    catch (e: DiscordException)
    {
      L.error("MISC DISCORD ERROR FROM {}: {}", serverNameFromChannel(id), e.errorMessage)
    }
    catch (e: MissingPermissionsException)
    {
      L.error("WANBOT LACKS PERMISSIONS ON {}: {}", serverNameFromChannel(id), e.errorMessage)
    }
  }

  @EventSubscriber
  fun onMessageReceivedEvent(event: MessageReceivedEvent)
  {
    if (event.author == client.ourUser)
    {
      L.warn("WANBOT DON'T NEED NO CALLBACKS FROM OURSELF")
      return
    }
    L.warn("GOT {} BY {} ON CHANNEL {} VIA DISCORD", event.message.content, event.author.name, event.channel.name)
    processInput(event.message.content, event.author.name, false, event.author.longID, event.channel.longID)
  }


  private fun serverNameFromChannel(channel: IChannel): String
  {
    return if (channel.guild == null) "[direct]" else channel.guild.name
  }
}
