
# name cycle header words[dict]
proc simulate-message { args } {
  set sentence "(<sentence> ^word"
  set rhs "(<in> ^message <msg>)
           (<msg> ^header <h> ^sentence <sentence>)
           [simulate-header-rhs [dict get $args header]]"
  set words [dict get $args words]
  set last [llength $words]

  if { $last <= 0 } { error "Must have at least one word" }

  set last [expr $last - 1]

  set id 0
  foreach word $words {

    set keyword [dict get $word keyword]
    set token [dict get $word token]
    set lemma [dict get $word lemma]
    set tag [dict get $word tag]
    set chunk [dict get $word chunk]
    append sentence " <word$id>"
    append rhs "(<word$id> ^id $id ^keyword $keyword ^token $token ^lemma $lemma ^tag $tag ^chunk $chunk"

    if { $id == 0 } {
      append rhs " ^first *yes*"
    } else {
      append rhs " ^previous <word[expr $id - 1]>"
    }
    if { $id >= $last } {
      append rhs " ^last *yes*"
    } else {
      append rhs " ^next <word[expr $id + 1]>"
    }
    append rhs ")\n"

    incr id
  }

  append sentence ")"

  append rhs $sentence

  set name [dict get $args name]

  simulate-input $name [dict get $args cycle] $rhs
}

# name cycle header delay[int]
proc simulate-incubate { args } {
  set rhs "(<in> ^incubate <inc>)
           (<inc> ^header <h> ^delay [dict get $args delay])
           [simulate-header-rhs [dict get $args header]]"
  set name "[dict get $args name]*delay-[dict get $args delay]"

  simulate-input $name [dict get $args cycle] $rhs
}

# name cycle header user_name[string] user_id[int] online[bool]
proc simulate-user-status { args } {
  set online_soar "*no*"
  if { [dict get $args online] } {
    set online_soar "*yes*"
  }
  set user_id [dict get $args user_id]
  set user_name [dict get $args user_name]
  set name "$user_name*user-$user_id*online-[dict get $args online]"
  set rhs "(<in> ^user-status <user-status>)
           (<user-status> ^header <h> ^global-id $user_id ^name [string tolower $user_name] ^print $user_name ^online $online_soar)
           [simulate-header-rhs [dict get $args header]]"
  simulate-input $name [dict get $args cycle] $rhs
}

# server[string] channel[int] user[int]
proc simulate-header-rhs { header } {
  return "(<h> ^server [dict get $header server] ^channel [dict get $header channel] ^user [dict get $header user])"
}

# name cycle header target[user-id] wit[string] incubate[int?]
proc simulate-insult-goal { args } {
  if { [dict exists $args incubate] } {
    set incubate [dict get $args incubate]
  } else {
    set incubate 0
  }
  set name "[dict get $args name]*target-[dict get $args target]*wit-[dict get $args wit]"
  set rhs "(<goals> ^insult <g>)
           (<g> ^meta <m> ^header <h> ^user [dict get $args target] ^word |[dict get $args wit]|)
           (<m> ^incubate $incubate ^created.why |Simulated insult goal| ^what |Insult user|)
           [simulate-header-rhs [dict get $args header]]"
  simulate-input $name [dict get $args cycle] $rhs
}

proc simulate-input { name cycle rhs } {
  variable interruptoninput
  set function ""
  if { [info exists interruptoninput] && $interruptoninput } {
    set function "(interrupt)"
  }
  sp "soarunit*simulate-input*$name*cycle-$cycle
     (state <s> ^name wanbot ^io.input-link <in> ^goals <goals>)
     (<in> -^cycle-count < $cycle)
  -->
     (write (crlf) |*** Simulating $name ***|)
     $rhs
     $function"
}
