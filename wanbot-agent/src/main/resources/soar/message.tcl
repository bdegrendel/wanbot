# Tcl code supporting message handling

echo "Loading message.tcl..."

# How many seconds to delay between comprehend (usually immediate) and actually responding.
if { ! [info exists respond_delay] } {
  set respond_delay 3
}
