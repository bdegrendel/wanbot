package com.degrendel.wanbot.nlp

import com.degrendel.wanbot.common.logger
import opennlp.tools.chunker.ChunkerME
import opennlp.tools.chunker.ChunkerModel
import opennlp.tools.lemmatizer.DictionaryLemmatizer
import opennlp.tools.postag.POSModel
import opennlp.tools.postag.POSTaggerME
import opennlp.tools.sentdetect.SentenceDetectorME
import opennlp.tools.sentdetect.SentenceModel
import opennlp.tools.tokenize.TokenizerME
import opennlp.tools.tokenize.TokenizerModel
import java.util.*

class NaturalLanguageProcessor
{
  companion object
  {
    val L by logger()
  }

  private val ignorableTags = arrayOf(".", "''", "``", ",,", ",")

  init
  {
    L.info("LOADING MODELS FOR PARSING INFERIOR HUMAN LANGUAGE....")
  }

  private val sentenceDetecter = SentenceDetectorME(SentenceModel(javaClass.getResourceAsStream("/nlp/en-sent.bin")))
  private val tokenizer = TokenizerME(TokenizerModel(javaClass.getResourceAsStream("/nlp/en-token.bin")))
  private val tagger = POSTaggerME(POSModel(javaClass.getResourceAsStream("/nlp/en-pos-maxent.bin")))
  private val lemmatizer = DictionaryLemmatizer(javaClass.getResourceAsStream("/nlp/en-lemmatizer.dict"))
  private val chunker = ChunkerME(ChunkerModel(javaClass.getResourceAsStream("/nlp/en-chunker.bin")))

  init
  {
    L.info("NLP IS READY, FLESHBAGS")
  }

  @Synchronized
  fun parse(message: String, debugLog: Boolean = false): List<Sentence>
  {
    var id = 0
    val results = ArrayList<Sentence>()
    val sentences = sentenceDetecter.sentDetect(message)

    for (s in sentences)
    {
      val parsed = Sentence(s, ArrayList())
      // OpenNLP (or at least the default trained libraries) will sometimes drop the final word(s) if the sentence
      // doesn't end in a sentence ending punctuation mark.  To help it out, add a full stop if needed.
      // (Grammar check: '!?.' are the only options for ending a sentence, right?)
      val sentence = if (Regex("[!?.]$").find(s) != null) s else s + "."

      if (debugLog)
        L.info("SENTENCE '{}':", sentence)

      val tokens = tokenizer.tokenize(sentence)
      val tags = tagger.tag(tokens)
      val lemmas = lemmatizer.lemmatize(tokens, tags)
      val chunks = chunker.chunk(tokens, tags)
      for (i in 0 until tokens.size)
      {
        if (debugLog)
          L.info("{} --> {}, {}, {}", i, tokens[i], tags[i], lemmas[i])
        if (tags[i] in ignorableTags)
          continue
        val keyword = if (lemmas[i] != "O") lemmas[i].toLowerCase() else tokens[i].toLowerCase()
        parsed.words.add(Word(id++, keyword, tokens[i], tags[i], lemmas[i], chunks[i]))
        if (debugLog)
          L.info("   --> {}", parsed.words.last())
      }
      if (parsed.words.isNotEmpty())
        results.add(parsed)
    }
    return results
  }

  fun debugDump(sentence: Sentence)
  {
    if (sentence.words.isNotEmpty())
    {
      val ss = StringBuilder()
      ss.append("{\n")
      for (word in sentence.words)
      {
        ss.append("{ keyword |${word.keyword}| token |${word.token}| lemma ${word.lemma} tag ${word.tag} chunk ${word.chunk} }\n")
      }
      ss.append("}")
      System.out.println(ss.toString())
    }
  }

  fun replLoop()
  {
    L.info("ENTERING NLP REPL")
    try
    {
      val scanner = Scanner(System.`in`)

      while (true)
      {
        System.out.print(">")
        val line = scanner.nextLine()

        if (line.toLowerCase() == "!halt")
        {
          L.info("!HALTING")
          break
        }
        else if (line.startsWith("!"))
        {
          L.info("???")
          continue
        }

        for (sentence in parse(line, true))
        {
          L.info("New sentence:")
          debugDump(sentence)
        }
      }

    }
    catch (e: IllegalStateException)
    {
      L.info("ILLEGALSTATEEXCEPTION {}, ASSUMING STDIN->EOF", e.localizedMessage)
    }
  }
}
