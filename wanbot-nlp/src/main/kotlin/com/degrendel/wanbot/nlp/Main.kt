package com.degrendel.wanbot.nlp

import com.degrendel.wanbot.common.RedisConnection
import joptsimple.OptionParser
import org.slf4j.LoggerFactory
import java.net.URI
import java.util.*

private val L = LoggerFactory.getLogger("com.degrendel.wanbot.nlp.MainKt")!!

fun main(args: Array<String>)
{
  L.info("NLP Loading")

  val parser = OptionParser()
  val helpOption = parser.acceptsAll(Arrays.asList("?", "h", "help")).forHelp()
  val redisOption = parser.acceptsAll(Arrays.asList("r", "redis")).withRequiredArg()
  val replOption = parser.acceptsAll(Arrays.asList("repl"))

  val options = parser.parse(*args)

  if (options.has(helpOption))
  {
    parser.printHelpOn(System.out)
    System.exit(0)
  }

  if (options.has(replOption))
  {
    Repl().run()
    System.exit(0)
  }

  val uri = if (options.has(redisOption))
    URI(options.valueOf(redisOption))
  else
    RedisConnection.DEFAULT
}
