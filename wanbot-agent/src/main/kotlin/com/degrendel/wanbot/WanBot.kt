package com.degrendel.wanbot

import com.degrendel.wanbot.agent.Agent
import com.degrendel.wanbot.common.RedisConnection
import com.degrendel.wanbot.common.logger
import com.degrendel.wanbot.nlp.NaturalLanguageProcessor
import com.degrendel.wanbot.output.Halt
import com.degrendel.wanbot.output.Recite
import com.degrendel.wanbot.server.DiscordServer
import com.degrendel.wanbot.server.LocalServer
import com.degrendel.wanbot.server.Server
import org.jsoar.kernel.events.OutputEvent
import org.jsoar.kernel.symbols.DoubleSymbol
import org.jsoar.kernel.symbols.Identifier
import org.jsoar.kernel.symbols.IntegerSymbol
import org.jsoar.kernel.symbols.StringSymbol
import java.net.URI

class WanBot(redisUri: URI, token: String, voice: Boolean)
{
  companion object
  {
    val L by logger()
  }

  private val local: LocalServer
  private val discord: DiscordServer?

  val agent = Agent(true)
  val nlp = NaturalLanguageProcessor()

  private val redis = RedisConnection(redisUri)

  private val goalStatusPublisher = GoalStatusPublisher(redis)

  private val output = Thread(Output())

  init
  {
    if (token.isBlank())
    {
      L.info("WANBOT IN SIMULATION MODE; MORTALS SPARED")
    }
    else
    {
      L.info("WANBOT CONNECTING TO DISCORD WITH KEY (censored), HITCHES")
    }

    local = LocalServer(this)

    discord = if (token.isNotBlank()) DiscordServer(this, token, voice) else null

    agent.agent.events.addListener(OutputEvent::class.java , { event ->
      if (event !is OutputEvent)
        L.error("Received event of not OutputEvent: {}, type:{}", event, event.javaClass)
      else if (event.changes.hasNext())
      {
        var goals: Identifier? = null
        for (wme in event.wmes)
        {
          if (wme.attribute.asString().value == "goals")
          {
            goals = wme.value.asIdentifier()
            break
          }
        }
        if (goals == null)
          L.warn("Unable to find goals in output link, not sending status!")
        else
          updateGoalStatus(goals)
      }
    })
  }

  inner class Output : Runnable
  {
    override fun run()
    {
      var running = true
      while (running)
      {
        val command = agent.output.take()

        when (command)
        {
          is Halt -> running = false
          is Recite -> lookupServer(command.server)?.speak(command.sentence, command.channel)
        }
      }
    }
  }

  private fun updateGoalStatus(root: Identifier)
  {
    val visited = HashSet<Identifier>()
    val goals = ArrayList<com.degrendel.wanbot.GoalStatusPublisher.Goal>()

    visited.add(root)

    for (wme in root.wmes)
    {
      val goal = com.degrendel.wanbot.GoalStatusPublisher.Goal(wme.attribute.asString().value, HashMap())
      val child = wme.value.asIdentifier()
      if (child == null)
        L.warn("Found goal with non identifier child: ({} ^{} {})", root, wme.attribute, wme.value)
      else
        recurseProcessWmeTree(child, visited, goal.data)
      goals.add(goal)
    }

    goalStatusPublisher.publish(goals)
  }

  private fun recurseProcessWmeTree(start: Identifier, visited: HashSet<Identifier>, children: HashMap<String, Any>)
  {
    visited.add(start)

    for (wme in start.wmes)
    {
      val attribute = wme.attribute.asString().value
      if (children.containsKey(attribute))
        L.warn("Duplicate WME ({} ^{}) existing: {} new: {}", start, attribute, children[attribute], wme.value)
      else
      {
        children[attribute] = when (wme.value)
        {
          is IntegerSymbol -> wme.value.asInteger().value
          is StringSymbol -> wme.value.asString().value
          is DoubleSymbol -> wme.value.asDouble().value
          is Identifier -> {
            val recursive = HashMap<String, Any>()
            recurseProcessWmeTree(wme.value.asIdentifier(), visited, recursive)
            recursive
          }
          else -> {
            L.warn("Unknown Symbol for: {}", wme)
          }
        }

      }
    }
  }

  private fun lookupServer(type: String): Server?
  {
    val enum = try
    {
      Server.ServerType.valueOf(type.toUpperCase())
    }
    catch (_: IllegalArgumentException)
    {
      L.error("UNKNOWN SERVER TYPE '{}'", type)
      return null
    }
    return when (enum)
    {
      Server.ServerType.LOCAL -> local
      Server.ServerType.DISCORD -> discord
    }
  }

  fun start(runAgentInDebugger: Boolean)
  {
    agent.start(runAgentInDebugger)
    local.start()
    output.start()
  }

  fun stop(user: String, reason: String)
  {
    L.info("WANBOT STOPPING DUE TO THE BEGGING OF {}; APPARENTLY '{}'", user, reason)
    agent.output.put(Halt(user, reason))
    local.stop()
    discord?.stop()
    agent.stop()
  }
}
