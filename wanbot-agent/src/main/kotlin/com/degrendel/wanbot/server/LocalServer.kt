package com.degrendel.wanbot.server

import com.degrendel.wanbot.ADMINISTRATOR_USERNAME
import com.degrendel.wanbot.WanBot
import com.degrendel.wanbot.common.logger
import com.degrendel.wanbot.input.Header
import com.degrendel.wanbot.input.UserStatus
import java.util.*

class LocalServer(parent: WanBot) : Server(parent, Server.ServerType.LOCAL)
{

  companion object
  {
    val L by logger()
  }

  var inputProcessor: Input? = null

  init
  {
    enableMagicCommands(MagicCommands.PING, MagicCommands.HALT, MagicCommands.DEBUGGER, MagicCommands.VISUALIZE,
        MagicCommands.INCUBATE)
  }

  inner class Input : Runnable
  {
    var running = true

    override fun run()
    {
      input.put(UserStatus(Header(ServerType.LOCAL.toString()), parent.agent.users.lookup("WanBot"), "wanbot",
          "WanBot", true))
      input.put(UserStatus(Header(ServerType.LOCAL.toString()), parent.agent.users.lookup(ADMINISTRATOR_USERNAME),
          ADMINISTRATOR_USERNAME.toLowerCase(), ADMINISTRATOR_USERNAME, true))
      try
      {
        val scanner = Scanner(System.`in`)

        while (running)
        {
          processInput(scanner.nextLine(), ADMINISTRATOR_USERNAME, true)
        }
      }
      catch (e: IllegalStateException)
      {
        parent.stop(ADMINISTRATOR_USERNAME, "(EOF on stdin)")
        L.info("ILLEGALSTATEEXCEPTION {}, ASSUMING STDIN->EOF", e.localizedMessage)
      }
    }
  }

  override fun start()
  {
    inputProcessor = Input()
    Thread(inputProcessor).start()
  }

  override fun stop()
  {
    inputProcessor?.running = false
    System.`in`.close()
  }

  override fun speak(message: String, channel: Long)
  {
    L.info("WANBOT SAYS {}", message.toUpperCase())
  }
}
