package com.degrendel.wanbot.nlp

import com.degrendel.wanbot.common.logger
import edu.stanford.nlp.ling.CoreLabel
import edu.stanford.nlp.simple.Document
import edu.stanford.nlp.trees.Tree
import java.util.*

class Repl
{
  companion object
  {
    val L by logger()
  }

  fun run()
  {
    L.info("ENTERING NLP REPL")
    try
    {
      val scanner = Scanner(System.`in`)

      while (true)
      {
        System.out.print(">")
        val line = scanner.nextLine()

        if (line.toLowerCase() == "!halt")
        {
          L.info("!HALTING")
          break
        }
        else if (line.startsWith("!"))
        {
          L.info("???")
          continue
        }

        val document = Document(line)
        for (sentence in document.sentences())
        {
          L.info("Sentence '{}'", sentence)
          L.info("Lemmas: {}" , sentence.lemmas())
          L.info("Parse: {}", sentence.parse())
          L.info("POS: {}", sentence.posTags())
          L.info("NER: {}", sentence.nerTags())
          dumpTree(sentence.parse(), 0)
        }
      }
    }
    catch (e: IllegalStateException)
    {
      L.info("ILLEGALSTATEEXCEPTION {}, ASSUMING STDIN->EOF", e.localizedMessage)
    }
  }

  private fun dumpTree(tree: Tree, level: Int)
  {
    val leaf = tree.isLeaf
    val value = tree.label() as CoreLabel
    val text = "word: ${value.word()} lemma: ${value.lemma()} tag: ${value.tag()} category: ${value.category()} " +
        "ner: ${value.ner()} sentIndex: ${value.sentIndex()} original: ${value.originalText()}"
    if (!leaf)
      System.out.println("(".padStart(level, ' ') + text)
    else
      System.out.print(text.padStart(level + 1, ' '))
    for (child in tree.childrenAsList)
      dumpTree(child, level + 1)
    if (!leaf)
      System.out.println(")".padStart(level, ' '))
  }
}
