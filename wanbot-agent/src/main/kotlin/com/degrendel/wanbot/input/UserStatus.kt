package com.degrendel.wanbot.input

data class UserStatus(val header: Header, val globalId: Int, val name: String, val print: String,
                      val online: Boolean) : Source
