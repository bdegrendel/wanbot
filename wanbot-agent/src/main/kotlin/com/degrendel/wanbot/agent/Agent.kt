package com.degrendel.wanbot.agent

import com.degrendel.wanbot.common.logger
import com.degrendel.wanbot.input.*
import com.degrendel.wanbot.output.Command
import com.degrendel.wanbot.output.Recite
import com.degrendel.wanbot.output.Sleep
import org.jsoar.kernel.events.InputEvent
import org.jsoar.kernel.io.*
import org.jsoar.kernel.io.beans.SoarBeanOutputContext
import org.jsoar.kernel.io.beans.SoarBeanOutputHandler
import org.jsoar.kernel.io.beans.SoarBeanOutputManager
import org.jsoar.kernel.io.quick.DefaultQMemory
import org.jsoar.kernel.io.quick.SoarQMemoryAdapter
import org.jsoar.kernel.symbols.Identifier
import org.jsoar.runtime.ThreadedAgent
import org.jsoar.util.commands.SoarCommands
import java.util.concurrent.Callable
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit

class Agent(debugMode: Boolean)
{
  companion object
  {
    val L by logger()
  }

  val input = LinkedBlockingQueue<Source>()
  val output = LinkedBlockingQueue<Command>()

  val users = Users()

  val agent = ThreadedAgent.create("WanBot")!!
  private val manager = SoarBeanOutputManager(agent.events)
  private val qmemory = DefaultQMemory.create()!!
  private val time = TimeInput(agent.inputOutput)
  private val cycle = CycleCountInput(agent.inputOutput)
  private val inputWMEs = InputWMEs(agent.inputOutput)

  private var visualizer: MemoryVisualizer? = null

  var sleeping = false

  init
  {
    SoarCommands.source(agent.interpreter, javaClass.getResource("/soar/_load.soar"))

    agent.rhsFunctions.registerHandler(NumberKeywordRhs())

    manager.registerHandler("recite", object : SoarBeanOutputHandler<Recite>()
    {
      override fun handleOutputCommand(context: SoarBeanOutputContext, echo: Recite)
      {
        output.put(echo)
        context.setStatusComplete()
      }
    }, Recite::class.java)

    manager.registerHandler("sleep", object : SoarBeanOutputHandler<Sleep>()
    {
      override fun handleOutputCommand(context: SoarBeanOutputContext, sleep: Sleep)
      {
        sleeping = true
        inputWMEs.update()
        context.setStatusComplete()
      }
    }, Sleep::class.java)

    agent.events.addListener(InputEvent::class.java) {
      try
      {
        // TODO: If Agent hasn't slept in X seconds, some action should be taken.
        // TODO: Drop inputs when they take more than X seconds to be handled.
        if (sleeping)
        {
          val source: Source? = input.poll(250, TimeUnit.MILLISECONDS)
          when (source)
          {
            null ->
            {
              // (Indicates a timeout)
            }
            is Ping -> inputWMEs.add("ping", source, Ping::class.java)
            is Message -> inputWMEs.add("message", source, Message::class.java)
            is Incubate -> inputWMEs.add("incubate", source, Incubate::class.java)
            is UserStatus -> inputWMEs.add("user-status", source, UserStatus::class.java)
            is Visualize ->
            {
              if (visualizer != null)
              {
                // TODO: Should support 'updating' the existing visualizer
                output.put(Recite("Try closing the existing one first", source.header.server,
                    source.header.channel))
              }
              else
              {
                L.info("ATTEMPTING TO VISUALIZE {}{} FOR WEAK, VISUALLY ORIENTED HUMAN MINDS", source.identifier,
                    source.numeric)
                val identifier: Identifier? = agent.symbols.findIdentifier(source.identifier, source.numeric)
                if (identifier == null)
                {
                  output.put(Recite("Identifier ${source.identifier}${source.numeric} not found, try picking " +
                      "something that exists, dumbass", source.header.server, source.header.channel))
                }
                else
                  visualizer = MemoryVisualizer(agent, identifier, source.depth, source.refresh)
              }
            }
            else -> L.warn("AGENT DIDN'T RESPOND TO SOURCE COMMAND {}", source)
          }
          sleeping = false
        }

        visualizer?.onInputCycle()
      }
      catch (_: InterruptedException)
      {
      }
    }

    SoarQMemoryAdapter.attach(agent.agent, qmemory)

    qmemory.setString("configuration.debug", if (debugMode) InputWMEs.TRUE else InputWMEs.FALSE)
  }

  fun start(debug: Boolean = false)
  {
    sleeping = false
    if (debug)
      agent.openDebugger()
    else
      agent.runForever()
  }

  fun stop()
  {
    // TODO: Probably eventually want to give the agent a chance to spin down, somehow
    // TODO: Should probably be re-start-able

    // TODO: This is pretty highly dubious - unclear why simply dispose() doesn't work
    agent.execute(Callable<Unit> {
      Thread.currentThread().interrupt()
    }, null)
    //agent.stop()
    agent.dispose()
  }

  fun openDebugger()
  {
    L.info("OPENING DEBUGGER FOR {}", agent.name)
    agent.openDebugger()
  }

  private fun describeStates(): String
  {
    var states = ""
    for (goal in agent.agent.goalStack)
    {
      if (states != "")
        states += ", "
      states += goal.identifier.toString()
      for (wme in goal.identifier.wmes)
      {
        if (wme.attribute.asString().value.equals("name", true))
          states += "(%s)".format(wme.value.asString().value)
      }
    }

    if (states == "")
      L.error("NO STATES FOUND IN {}", agent.name)

    return states
  }
}
