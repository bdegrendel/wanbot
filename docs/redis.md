# Redis

## User Status

user:\<server:Enum[Local,Discord]\>#\<id:Int\>

* :Online -> Boolean
* :Channels -> Set of Channel IDs this user belongs to
* :Global -> Integer

## User Global

user:\<id:Int\> -> Hash of Server to local IDs

## Voice Recognition Queue

input:voice -> List of Json Utterance

## NLP Parser Queue

input:nlp -> List of Json Unparsed Messages

## Received Messages Queue

input:messages -> List of Json Parsed Messages

## Responses Queue

output:\<server:Enum[Local,Discord]\> -> List of Json generated responses

## Goal Status

goals:type#\<id:Int\> -> Hash of Goal Data
goals:type#\<id:Int\>:action#\<id:Int\> -> Hash of Goal Action Data
