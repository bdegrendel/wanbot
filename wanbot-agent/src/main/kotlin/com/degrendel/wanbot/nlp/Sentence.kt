package com.degrendel.wanbot.nlp

data class Sentence(val raw: String, val words: ArrayList<Word>)
