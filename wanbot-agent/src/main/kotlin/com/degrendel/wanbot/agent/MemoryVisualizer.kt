package com.degrendel.wanbot.agent

import com.degrendel.wanbot.common.logger
//import com.mxgraph.layout.mxCircleLayout
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout
import com.mxgraph.layout.mxCompactTreeLayout
//import com.mxgraph.layout.mxGraphLayout
//import com.mxgraph.layout.mxOrganicLayout
import com.mxgraph.swing.mxGraphComponent
import org.jgrapht.ext.JGraphXAdapter
import org.jsoar.kernel.symbols.Identifier
import java.awt.BorderLayout
import javax.swing.JFrame
import javax.swing.JLabel
import org.jgrapht.graph.*
import org.jsoar.kernel.symbols.Symbol
import org.jsoar.runtime.ThreadedAgent
import javax.swing.WindowConstants


class MemoryVisualizer(agent: ThreadedAgent, private val identifier: Identifier,
                       private val maxDepth: Int = UNLIMITED_DEPTH, private val refreshRate: Int = DO_NOT_REFRESH)
  : JFrame("WanBot: State Visualizer")
{
  companion object
  {
    val L by logger()
    const val UNLIMITED_DEPTH = -1
    const val DO_NOT_REFRESH = -1
  }

  private class Edge(val identifier: Identifier, val attribute: Symbol)
  {
    private val display = attribute.toString()

    override fun toString(): String
    {
      return display
    }
  }

  private inner class Vertex(val symbol: Symbol, val container: Identifier)
  {
    override fun toString(): String
    {
      return symbol.toString()
    }
  }

  private var cycle = 0

  private val graph = DefaultListenableGraph(DirectedPseudograph<Any, Edge>(Edge::class.java))
  private val adapter = JGraphXAdapter(graph)
  //private val layout = mxOrganicLayout(adapter)
  private val layout = mxHierarchicalLayout(adapter)
  //private val layout: mxGraphLayout = mxCompactTreeLayout(adapter)

  private val component = mxGraphComponent(adapter)

  init
  {
    L.info("Visualizing {} refresh:{} depth:{}", identifier, refreshRate, maxDepth)
    isVisible = true

    if (layout is mxCompactTreeLayout)
      layout.levelDistance = 100

    defaultCloseOperation = WindowConstants.DISPOSE_ON_CLOSE
    contentPane.add(JLabel("Displaying state $identifier"), BorderLayout.NORTH)
    component.isEnabled = false
    contentPane.add(component, BorderLayout.CENTER)

    refresh()

    pack()
    extendedState = (extendedState or JFrame.MAXIMIZED_BOTH)
  }

  fun onInputCycle()
  {
    if (refreshRate == DO_NOT_REFRESH) return

    if (cycle >= refreshRate)
    {
      cycle = 0
      refresh()
    }
    else
      cycle++
  }

  private fun refresh()
  {
    L.info("Refreshing {}", identifier)
    // TODO: Whoa nelly this is inefficient
    val vertices = HashSet(graph.vertexSet())
    graph.removeAllVertices(vertices)
    val edges = HashSet(graph.edgeSet())
    graph.removeAllEdges(edges)
    graph.addVertex(identifier)
    recursiveRefresh(identifier, HashSet(), 0, maxDepth)

    layout.execute(adapter.defaultParent)
  }

  private fun recursiveRefresh(identifier: Identifier, existing: HashSet<Identifier>, depth: Int, maxDepth: Int)
  {
    existing.add(identifier)
    for (wme in identifier.wmes)
    {
      if (wme.attribute.asString().value == "superstate")
        continue

      val value = wme.value
      val vertex: Any = value as? Identifier ?: Vertex(value, identifier)

      graph.addVertex(vertex)
      graph.addEdge(identifier, vertex, Edge(identifier, wme.attribute))
      if (value is Identifier && value !in existing)
      {
        if (depth < maxDepth || maxDepth < 0)
          recursiveRefresh(value, existing, depth + 1, maxDepth)
      }
    }
  }
}
