package com.degrendel.wanbot.agent

// TODO: Implement less naive, more robust way of looking up users
class Users
{
  private val ids = HashMap<String, Int>()

  private var nextId = 0

  fun lookup(username: String): Int
  {
    val key = username.toLowerCase()
    val id = ids[key]

    if (id == null)
    {
      val newId = nextId++
      ids.put(key, newId)
      return newId
    }
    return id
  }
}
