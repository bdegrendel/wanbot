package com.degrendel.wanbot

import com.degrendel.wanbot.common.RedisConnection
import com.degrendel.wanbot.common.logger
import com.google.gson.GsonBuilder

class GoalStatusPublisher(redis: RedisConnection)
{
  companion object
  {
    val L by logger()
    const val CHANNEL = "wanbot-agent-goal-status"
  }

  private val channel = redis.Channel(CHANNEL)
  private val gson = GsonBuilder().setPrettyPrinting().create()!!

  data class Goal(val type: String, val data: HashMap<String, Any>)

  fun publish(status: ArrayList<Goal>)
  {
    channel.publish(gson.toJson(status))
  }
}
