package com.degrendel.wanbot

import com.degrendel.wanbot.common.RedisConnection
import com.degrendel.wanbot.nlp.NaturalLanguageProcessor
import org.apache.commons.cli.*
import org.apache.commons.io.FileUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.net.URI
import java.nio.charset.Charset

const val ADMINISTRATOR_USERNAME = "ADMINATOR"

private val L = LoggerFactory.getLogger("com.degrendel.wanbot.MainKt")!!

fun main(args: Array<String>)
{
  L.warn("WANBOT IN THE HOUSE, BEACH")

  val options = Options()

  // -t, --token TOKEN
  options.addOption(Option.builder("t").longOpt("token").hasArg()
      .desc("List token on the command (beware of security issues)").build())

  // -f, --filename FILENAME
  options.addOption(Option.builder("f").longOpt("filename").hasArg()
      .desc("Filename containing discord token").build())

  // -V, --voice
  options.addOption(Option.builder("V").longOpt("voice")
      .desc("Connect to voice channels in discord").build())

  // -h, --help
  options.addOption(Option.builder("h").longOpt("help")
      .desc("Display this full help statement").build())

  // -d, --debugger
  options.addOption(Option.builder("d").longOpt("debugger")
      .desc("Start the agent paused in the debugger").build())

  // --nlp-repl
  options.addOption(Option.builder().longOpt("nlp-repl")
      .desc("Enables input -> NLP parsed results loop; does not open the agent").build())

  // -r, --redis URI
  options.addOption(Option.builder("r").longOpt("redis").hasArg()
      .desc("Connect to a Redis server at this URI").build())

  try
  {
    val parse = DefaultParser().parse(options, args)
    if (parse.hasOption("h"))
    {
      HelpFormatter().printHelp("WanBot [arguments]", options)
      System.exit(0)
    }

    if (parse.hasOption("nlp-repl"))
    {
      NaturalLanguageProcessor().replLoop()
      System.exit(0)
    }

    if (parse.hasOption("t") && parse.hasOption("f"))
    {
      WanBot.L.error("Cannot specify both --token and --filename, pick one or the other!")
      System.exit(1)
    }

    val token = when
    {
      parse.hasOption("t") -> parse.getOptionValue("t").trim()
      parse.hasOption("f") -> FileUtils.readFileToString(File(parse.getOptionValue("f")), Charset
          .defaultCharset()).trim()
      else -> ""
    }
    val localAgentDebugger = parse.hasOption("d")
    val voice = parse.hasOption("V")

    val redis = if (parse.hasOption("r"))
      URI(parse.getOptionValue("r"))
    else
      RedisConnection.DEFAULT

    val wanbot = WanBot(redis, token, voice)

    wanbot.start(localAgentDebugger)
  }
  catch (e: ParseException)
  {
    L.error("Unable to parse command line arguments: {}; try --help", e.localizedMessage)
    System.exit(1)
  }
}
