package com.degrendel.wanbot.agent

import org.jsoar.kernel.rhs.functions.AbstractRhsFunctionHandler
import org.jsoar.kernel.rhs.functions.RhsFunctionContext
import org.jsoar.kernel.rhs.functions.RhsFunctionException
import org.jsoar.kernel.rhs.functions.RhsFunctions
import org.jsoar.kernel.symbols.Symbol

class NumberOfEdgesRhs : AbstractRhsFunctionHandler(FUNCTION_NAME, 1, 1)
{
  companion object
  {
    const val FUNCTION_NAME = "number-of-edges"
  }

  override fun execute(context: RhsFunctionContext, arguments: List<Symbol>): Symbol
  {
    RhsFunctions.checkArgumentCount(this, arguments)

    val source = arguments[0].asIdentifier()
        ?: throw RhsFunctionException("$FUNCTION_NAME takes 1 argument: Identifier")

    var count: Long = 0

    for (wme in source.wmes)
      count++

    return context.symbols.createInteger(count)
  }
}
