package com.degrendel.wanbot.input

data class Incubate(val header: Header, val delay: Int) : Source
