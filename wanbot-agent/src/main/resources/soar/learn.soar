sp {learn*prefer*learn*to*respond
    "Prefers learn operator to respond, since we should respond while there's new facts aboot"
   (state <s> ^name message ^operator <o1> + ^operator <o2> +)
   (<o1> ^class learn)
   (<o2> ^class respond)
-->
   (<s> ^operator <o1> > <o2>)
}

sp {learn*propose*new*singular*noun
    "Proposes learning a new singular noun"
   (state <s> ^name message ^message.sentence.chunk.noun <noun> -^factuals.nouns.<type>.noun.name <name>)
   (<noun> ^name <name> ^amount singular ^type <type>)
-->
   (<s> ^operator <o> + =)
   (<o> ^class learn ^name learn-new-noun ^noun <noun>)
}

sp {learn*apply*learn*new-noun
    "Applies learning a new noun"
   (state <s> ^name message ^operator <o> ^factuals.nouns.<type> <nouns>)
   (<o> ^name learn-new-noun ^noun <noun>)
   (<noun> ^name <name> ^type <type>)
-->
   (<nouns> ^noun <n>)
   (<n> ^name <name> ^is <is>)
   (write (crlf) |Learning new | <type> | noun | <name>)
}

sp {learn*propose*new*noun-is-noun
    "Proposes learning a new 'NOUN is NOUN' pairing"
   (state <s> ^name message ^message.sentence.chunk.is <is>)
   (<is> ^subject.noun <subject> ^object.noun <object> -^inquiry)
   (<s> ^factuals.nouns.<subject-type>.noun <existing-subject> ^factuals.nouns.<object-type>.noun <existing-object>)
   (<subject> ^name <subject-name> ^type <subject-type>)
   (<object> ^name <object-name> ^type <object-type>)
   (<existing-object> ^name <object-name>)
   (<existing-subject> ^name <subject-name> -^is.noun <existing-object>)
-->
   (<s> ^operator <o> + =)
   (<o> ^class learn ^name learn-noun-is-noun ^subject <existing-subject> ^object <existing-object>)
}

sp {learn*apply*learn*new*noun-is-noun
    "Applies learning a new 'NOUN is NOUN' pairing"
   (state <s> ^name message ^operator <o>)
   (<o> ^name learn-noun-is-noun ^subject <sub> ^object <object>)
   # (Logging rules)
   (<sub> ^is <subject> ^name <sub-name>)
   (<object> ^name <obj-name>)
-->
   (<subject> ^noun <object>)
   (write (crlf) |Learning that | <sub-name> | is noun | <obj-name>)
}

sp {learn*propose*new*noun-is-adjective
    "Proposes learning a new 'NOUN is ADJECTIVE' pairing"
   (state <s> ^name message ^message.sentence.chunk.is <is>)
   (<is> ^subject.noun <subject> ^object.adjective <adjective> -^inquiry)
   (<s> ^factuals.nouns.<subject-type>.noun <existing-subject>)
   (<subject> ^name <subject-name> ^type <subject-type>)
   (<existing-subject> -^is.adjective.name <adjective-name>)
-->
   (<s> ^operator <o> + =)
   (<o> ^class learn ^name learn-noun-is-adjective ^subject <existing-subject> ^adjective <adjective>)
}

sp {learn*apply*learn*new*noun-is-adjective
    "Applies learning a new 'NOUN is ADJECTIVE' pairing"
   (state <s> ^name message ^operator <o>)
   (<o> ^name learn-noun-is-adjective ^subject <sub> ^adjective <adjective>)
   (<sub> ^is <subject> ^name <sub-name>)
   (<adjective> ^name <adj-name>)
-->
   (<subject> ^adjective <adjective>)
   (write (crlf) |Learning that | <sub-name> | is | <adj-name>)
}
