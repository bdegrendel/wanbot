package com.degrendel.wanbot.input

data class Visualize(val header: Header, val identifier: Char, val numeric: Long, val refresh: Int, val depth: Int)
  : Source
