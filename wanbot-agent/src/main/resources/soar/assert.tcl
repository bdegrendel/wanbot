
echo "Loading assert.tcl..."

if { [info exists soarunit] && $soarunit } {
  set assertfailbehavior "(fail)"
} else {
  set assertfailbehavior "(interrupt)"
}

# Generates rule asserting some lhs condition does not occur
proc assert { scope state name lhs msg } {
  variable assertfailbehavior
  sp "$scope*assert*$name
     (state <s> ^name $state ^top-state.debug *yes*)
     $lhs
  -->
     (write (crlf) |$msg|)
     $assertfailbehavior
  "
}

# Generates rule asserting that object has a ^field
proc assert-exists { scope state object field } {
  set lhs "(<s> ^$object <object>)
          -(<object> ^$field)"
  set name "[string map { . * } $object]*has*$field"
  set msg "$object must have at least one ^$field"
  assert $scope $state $name $lhs $msg
}

# Generates rule asserting object does not have duplicate ^field
proc assert-singular { scope state object field } {
  set lhs "(<s> ^$object <object>)
           (<object> ^$field <f1> {<> <f1>})"
  set name "[string map { . * } $object]*has*no*duplicate*$field"
  set msg "$object must have no more than one ^$field"
  assert $scope $state $name $lhs $msg
}

# Generates rule assert object has exactly one ^field
proc assert-unique { scope state object field } {
  set lhs "(<s> ^$object <object>)
          -{
             (<object> ^$field)
            -(<object> ^$field <f1> { <> <f1> })
           }"
  set name "[string map { . * } $object]*has*exactly*one*$field"
  set msg "$object must have exactly one ^$field"
  assert $scope $state $name $lhs $msg
}

# Generates rule assert time object is not directly linked to the input
proc assert-time-deep-copied { scope state object } {
  set lhs "(<s> ^$object <time> ^io.input-link.time <time>)"
  set name "[string map { . * } $object]*deep-copy*from*input"
  set msg "$object must (deep-copy) the time from the input link"
  assert $scope $state $name $lhs $msg
}

# Generates rule asserting ^field is an integer, if it exists.
proc assert-integer { scope state object field } {
  set lhs "(<s> ^$object <object>)
           (<object> ^$field <f> -^$field {<f> <=> 0})"
  set name "[string map { . * } $object]*$field*is*integer"
  set msg "$object can only have integer ^$field"
  assert $scope $state $name $lhs $msg
}

# Generates rule asserting ^field is a boolean (*yes*/*no*), if it exists.
proc assert-boolean { scope state object field } {
  set lhs "(<s> ^$object <object>)
           (<object> ^$field <value> -^$field {<value> << *yes* *no* >>})"
  set name "[string map { . * } $object]*$field*is*boolean"
  set msg "$object can only have boolean ^$field"
  assert $scope $state $name $lhs $msg
}

# Generates rule asserting field is an id of a valid user.
proc assert-is-user-id { scope state object field } {
  set lhs "(<s> ^$object <object> ^facts.users <users>)
           (<object> ^$field <value>)
          -(<users> ^user.id <value>)"
  set name "[string map { . * } $object]*$field*is*valid*user*id"
  set msg "$object must have a valid user id at ^$field"
  assert $scope $state $name $lhs $msg
}
