package com.degrendel.wanbot.agent

import com.degrendel.wanbot.common.logger
import org.jsoar.kernel.rhs.functions.AbstractRhsFunctionHandler
import org.jsoar.kernel.rhs.functions.RhsFunctionContext
import org.jsoar.kernel.rhs.functions.RhsFunctionException
import org.jsoar.kernel.symbols.Symbol

class NumberKeywordRhs : AbstractRhsFunctionHandler(FUNCTION_NAME, 1, 1)
{
  companion object
  {
    const val FUNCTION_NAME = "keyword-to-number"
    private val L by logger()
  }

  private val primatives = hashMapOf(
      1L to "one",
      2L to "two",
      3L to "three",
      4L to "four",
      5L to "five",
      6L to "six",
      7L to "seven",
      8L to "eight",
      9L to "nine",
      10L to "ten",
      20L to "twenty",
      30L to "thirty",
      40L to "forty",
      50L to "fifty",
      60L to "sixty",
      70L to "seventy",
      80L to "eighty",
      90L to "ninety"
  )

  private val specials = hashMapOf(
      0L to "zero",
      11L to "eleven",
      12L to "twelve",
      13L to "thirteen",
      14L to "fourteen",
      15L to "fifteen",
      16L to "sixteen",
      17L to "seventeen",
      18L to "eighteen",
      19L to "nineteen"
  )

  private val lookups = HashMap<String, Long>()

  init
  {
    // TODO: Whoa baby, this algorithm needs some work
    for (i in 1L..99L) {
      var text = ""
      val special = specials[i]
      if (special != null)
        lookups[special] = i

      var remaining = i

      while (remaining > 0)
      {
        if (remaining < 10)
        {
          text += primatives[remaining]!!
          remaining -= remaining
        }
        else if (remaining < 100)
        {
          val offset = remaining % 10
          val tens = remaining - offset

          text += primatives[tens]!!

          remaining = offset
        }
      }

      lookups[text] = i
    }
  }

  override fun execute(context: RhsFunctionContext, arguments: MutableList<Symbol>): Symbol
  {
    val input = arguments[0].asString().value ?: throw RhsFunctionException("$FUNCTION_NAME takes a single string argument")
    val d = input.toDoubleOrNull()
    if (d != null) return context.symbols.createDouble(d)

    val i = input.toLongOrNull()
    if (i != null) return context.symbols.createInteger(i)

    val lookup = lookups[input.replace("-", "")]

    if (lookup != null) return context.symbols.createInteger(lookup)

    return context.symbols.createString("nil")
  }
}
