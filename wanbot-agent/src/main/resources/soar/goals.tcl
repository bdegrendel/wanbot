echo "Loading goals.tcl..."

# Proposes and applies a top priority (+ = >) operator that creates an action for this substate.
proc propose-apply-start-goal-action { substate what } {
  sp "$substate*propose*start-action
     (state <s> ^name $substate ^results <results> ^goal.meta.actions <actions> -^action)
  -->
     (<s> ^operator <o> + = >)
     (<o> ^name start-action ^actions <actions> ^results <results>)
  "

  sp "$substate*apply*start-action
     (state <s> ^name $substate ^operator <o>)
     (<o> ^name start-action ^actions <actions> ^results <results>)
  -->
     (<actions> ^$substate <action>)
     (<action> ^what |$what| ^start <node>)
     (<results> ^action <action>)
     (<s> ^action <action>)
  "
}

proc match-goal { type } {
  return "(state <s> ^name wanbot ^goals.<$type> <goal>)
          (<goal> ^active *yes* ^meta <meta>)
          (<meta> ^actions <actions>)"
}

proc match-time-condition { } {
  return "(<s> ^io.input-link.time <time> ^io.input-link.cycle-count <cycle>)"
}

# issue-new-goal-rhs => Creates new goal
# match-goal => Generic ability to match goal
# match-goal-operator => Matches a selected top level single action operator
# issue-new-solo-operator-action => Creates new action due to a top level single action operator
# propose-goal-substate-operator => Proposes top level substate operator
# match-goal-substate-operator-before => Matches a selected top level substate operator before entering substate
# start-substate-operator-action => Creates new action due to a top level substate operator being selected
# match-goal-substate-operator-after => Matches a selected top level substate operator after substate completes

proc issue-new-goal { binding type what reason args } {
  set incubate ""
  if { [dict exists $args incubate] } {
    set incubate "^incubate [dict get $args incubate]"
  }

  set duration ""
  if { [dict exists $args duration] } {
    set duration "^duration [dict get $args duration]"
  }

  set inactive ""
  if { [dict exists $args inactive] && [dict get $args inactive] } {
    set inactive "^inactive *yes*"
  }

  return "(<goals> ^$type $binding)
          ($binding ^meta <metanew>)
          (<metanew> ^created <creatednew> ^what |$what| $incubate $duration $inactive)
          (<creatednew> ^cycle <cycle> ^time (deep-copy <time>) ^why |$reason|)"
}

proc achieve-goal-rhs { reason } {
  return "(<meta> ^achieve <achievenew>)
          (<achievenew> ^cycle <cycle> ^time (deep-copy <time>) ^why |$reason|)"
}

proc start-new-goal-action-rhs { binding type what } {
  return "(<actions> ^$type $binding)
          ($binding ^start <startnew> ^what |$what|)
          (<startnew> ^cycle <cycle> ^time (deep-copy <time>))"
}

proc end-goal-action-rhs { binding } {
  return "($binding ^end <endnew>)
          (<endnew> ^cycle <cycle> ^time (deep-copy <time>))"
}

set goalid 0
set goaldefaultduration 60
set goalexpiration 30

