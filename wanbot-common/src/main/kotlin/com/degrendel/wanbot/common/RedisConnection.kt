package com.degrendel.wanbot.common

import redis.clients.jedis.Jedis
import java.net.URI

class RedisConnection(uri: URI)
{
  companion object
  {
    val DEFAULT = URI("redis://localhost:6379")
  }
  private val jedis = Jedis(uri)

  inner class Channel(val name: String)
  {
    fun publish(message: String)
    {
      jedis.publish(name, message)
    }
  }
}
