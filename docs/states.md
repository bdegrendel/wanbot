# States

## Message

Handles processing of incoming messages.

## Comprehend

Turns raw input into more Soar-able data.

* Adds links between chunks
* Adds identifiers to chunks ([noun name: X adjective: Y])
* Proposes (low priority) classifies per sentence

## Classify

* Proposes "this is a BLANK sentence" based on structure.
* Has system for prioritizing more specific types
* Knows 'contexts' and proposes different possibilities based on 

## Start-Game-Selection

* Determines if we can start game selection
* Sets game-selection goal

## Complete-Game-Selection

* Counts up votes
* Proposes a few random selections
* Elaborates and returns a game
* Halts 'game-selection' goal
