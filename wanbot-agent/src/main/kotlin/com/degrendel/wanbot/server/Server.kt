package com.degrendel.wanbot.server

import com.degrendel.wanbot.WanBot
import com.degrendel.wanbot.agent.MemoryVisualizer
import com.degrendel.wanbot.input.*
import com.degrendel.wanbot.output.Recite

abstract class Server(val parent: WanBot, val type: ServerType)
{
  enum class ServerType(private val label: String)
  {
    LOCAL("local"),
    DISCORD("discord");

    override fun toString(): String = label
  }

  enum class MagicCommands(private val label: String)
  {
    HALT("halt"),
    PING("ping"),
    INCUBATE("incubate"),
    DEBUGGER("debugger"),
    VISUALIZE("visualize");

    override fun toString(): String = label
  }

  abstract fun start()
  abstract fun stop()
  abstract fun speak(message: String, channel: Long)

  private val magicCommands = HashSet<MagicCommands>()

  protected val input = parent.agent.input
  protected val output = parent.agent.output

  fun enableMagicCommands(vararg commands: MagicCommands)
  {
    commands.mapTo(magicCommands) { it }
  }

  fun processInput(message: String, author: String, direct: Boolean, user: Long = 0, channel: Long = 0)
  {
    val header = Header(type.toString(), user, channel)
    if (message.startsWith("!"))
    {
      var i = message.indexOf(" ")
      if (i < 0)
        i = message.length
      val command = try
      {
        MagicCommands.valueOf(message.substring(1, i).toUpperCase())
      }
      catch (_: IllegalArgumentException)
      {
        output.put(Recite("(What the hell does ${message.substring(0, i)} mean?)", type.toString(), channel))
        return
      }
      val payload = message.substring(i).trim()

      if (command !in magicCommands)
      {
        output.put(Recite("(Nuh uh uh, you didn't say the magic word, jackass)", type.toString(), channel))
        return
      }

      when (command)
      {
        MagicCommands.HALT ->
        {
          var reason = payload
          if (reason.isEmpty())
            reason = "(No reason given)"
          parent.stop(author, reason)
        }
        MagicCommands.PING -> input.put(Ping(header))
        MagicCommands.DEBUGGER -> parent.agent.openDebugger()
        MagicCommands.INCUBATE ->
        {
          val parameters = Regex("^(?<delay>\\d+)$").find(payload)
          if (parameters == null)
          {
            output.put(Recite("(!incubate needs an integer second delay, dumbass)", type.toString(), channel))
          }
          else
          {
            val delay = parameters.groups["delay"]!!.value.toInt()
            input.put(Incubate(header, delay))
          }
        }
        MagicCommands.VISUALIZE ->
        {
          val parameters = Regex("^(?<identifier>[a-zA-Z])(?<numeric>\\d+)\\s*(?<refresh>\\d+)?\\s*(?<depth>\\d+)?\$").find(payload)
          if (parameters == null)
          {
            output.put(Recite(
                "(The format, which you apparently are unaware, is !visualize identifier number [depth])",
                type.toString(), channel))
          }
          else
          {
            val identifier = parameters.groups["identifier"]!!.value.toUpperCase()[0]
            val numeric = parameters.groups["numeric"]!!.value.toLong()
            val refresh = if (parameters.groups["refresh"] != null) parameters.groups["refresh"]!!.value.toInt() else MemoryVisualizer.DO_NOT_REFRESH
            val depth = if (parameters.groups["depth"] != null) parameters.groups["depth"]!!.value.toInt() else MemoryVisualizer.UNLIMITED_DEPTH

            input.put(Visualize(header, identifier, numeric, refresh, depth))
          }
        }
      }
      return
    }
    else if (message.isNotBlank())
    {
      val parsed = parent.nlp.parse(message)
      input.put(Message(header, parsed))
    }
  }
}
