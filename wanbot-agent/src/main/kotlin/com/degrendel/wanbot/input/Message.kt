package com.degrendel.wanbot.input

import com.degrendel.wanbot.nlp.Sentence

data class Message(val header: Header, val sentences: List<Sentence>) : Source
